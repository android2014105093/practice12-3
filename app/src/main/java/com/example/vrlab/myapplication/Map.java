package com.example.vrlab.myapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by SSELAB on 2016-11-24.
 */

public class Map extends Fragment implements OnMapReadyCallback {

    private MapView mapView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        mapView = (MapView) rootView.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);
        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Make map’s setting here.
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new
                LatLng(35.886869, 128.608408), 16));

        googleMap.addMarker(new MarkerOptions().position(
                new LatLng(35.888664, 128.612122)).title("일청담").snippet("이일처엉다암"));
        googleMap.addMarker(new MarkerOptions().position(
                new LatLng(35.887984, 128.605098)).title("대운동장").snippet("대애우운도옹자앙"));
        googleMap.addMarker(new MarkerOptions().position(
                new LatLng(35.886133, 128.610106)).title("GS25").snippet("GGSS2255"));
    }
}